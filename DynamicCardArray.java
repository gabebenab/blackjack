import java.util.Random;

public class DynamicCardArray {
    private Card[] cards;
    private int numCards;

    public DynamicCardArray(int capacity) {
        cards = new Card[capacity];
        numCards = 0;
    }

    public void add(Card card) {
        if (numCards == cards.length) {
            Card[] newCards = new Card[numCards * 2];
            System.arraycopy(cards, 0, newCards, 0, numCards);
            cards = newCards;
        }
        cards[numCards] = card;
        numCards++;
    }

    public Card takeFromTop() {
        if (numCards == 0) {
            return null;
        }
        Card topCard = cards[numCards - 1];
        cards[numCards - 1] = null;
        numCards--;
        return topCard;
    }

    public Card takeFromBottom() {
        if (numCards == 0) {
            return null;
        }
        Card bottomCard = cards[0];
        System.arraycopy(cards, 1, cards, 0, numCards - 1);
        cards[numCards - 1] = null;
        numCards--;
        return bottomCard;
    }

    public void shuffle() {
        Random random = new Random();
        for (int i = numCards - 1; i > 0; i--) {
            int j = random.nextInt(i + 1);
            Card temp = cards[i];
            cards[i] = cards[j];
            cards[j] = temp;
        }
    }

    public boolean isEmpty() {
        return numCards == 0;
    }

    public int size() {
        return numCards;
    }

    public Card getCard(int index) {
        if (index < 0 || index >= numCards) {
            return null;
        }
        return cards[index];
    }
}