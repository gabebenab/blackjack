import java.util.Collections;

public class Deck {
    private final DynamicCardArray cardArray;
    
    public Deck() {
        this.cardArray = new DynamicCardArray();
        for (Suit suit : Suit.values()) {
            for (Value value : Value.values()) {
                cardArray.add(new Card(suit, value));
            }
        }
    }
    
    public void shuffle() {
        cardArray.shuffle();
    }
    
    public Card dealCard() {
        return cardArray.takeFromTop();
    }
    
    public boolean needsShuffle() {
        return cardArray.size() < 52;
    }
    
    public void merge(Deck other) {
        cardArray.merge(other.cardArray);
    }
    
    public int size() {
        return cardArray.size();
    }
    
    public void clear() {
        cardArray.clear();
    }
    
    public void print() {
        for (Card card : cardArray.getCards()) {
            System.out.println(card);
        }
    }
}