public class Hand {
    private final List<Card> cards;
    private int bet;
    private boolean isSplit;
    
    public Hand() {
        this.cards = new ArrayList<>();
        this.bet = 0;
        this.isSplit = false;
    }
    
    public void addCard(Card card) {
        cards.add(card);
    }
    
    public List<Card> getCards() {
        return Collections.unmodifiableList(cards);
    }
    
    public int getBet() {
        return bet;
    }
    
    public void setBet(int bet) {
        this.bet = bet;
    }
    
    public boolean isSplit() {
        return isSplit;
    }
    
    public void setSplit(boolean isSplit) {
        this.isSplit = isSplit;
    }
    
    public int getValue() {
        int value = 0;
        int aces = 0;
        
        for (Card card : cards) {
            if (card.getValue() == Value.ACE) {
                aces++;
            }
            value += card.getValue().getBlackjackValue();
        }
        
        while (value > 21 && aces > 0) {
            value -= 10;
            aces--;
        }
        
        return value;
    }
}