import java.util.Scanner;

public class Blackjack {
    private Deck deck;
    private DynamicCardArray dealerHand;
    private Player[] players;
    private int[] playerBets;
    private Scanner scanner;

    public Blackjack(int numPlayers) {
        deck = new Deck();
        dealerHand = new DynamicCardArray();
        players = new Player[numPlayers];
        playerBets = new int[numPlayers];
        scanner = new Scanner(System.in);

        for (int i = 0; i < numPlayers; i++) {
            System.out.print("Enter player " + (i+1) + "'s name: ");
            String name = scanner.nextLine();
            players[i] = new Player(name);
        }
    }

    public void play() {
        // Shuffle deck
        deck.shuffle();

        // Deal initial cards
        dealerHand.add(deck.takeFromTop());
        dealerHand.add(deck.takeFromTop());

        for (Player player : players) {
            player.getHand().add(deck.takeFromTop());
            player.getHand().add(deck.takeFromTop());
        }

        // Make bets
        for (int i = 0; i < players.length; i++) {
            System.out.print(players[i].getName() + ", place your bet: ");
            int bet = scanner.nextInt();
            players[i].placeBet(bet);
            playerBets[i] = bet;
        }

        // Play each player's turn
        for (int i = 0; i < players.length; i++) {
            playTurn(players[i]);
        }

        // Play dealer's turn
        playDealerTurn();

        // Determine winners and losers
        int dealerScore = getHandScore(dealerHand);
        for (int i = 0; i < players.length; i++) {
            int playerScore = getHandScore(players[i].getHand());
            if (playerScore > 21) {
                System.out.println(players[i].getName() + " busted with " + playerScore + " points!");
                players[i].loseBet();
            } else if (dealerScore > 21) {
                System.out.println(players[i].getName() + " wins with " + playerScore + " points! (Dealer busted)");
                players[i].winBet();
            } else if (playerScore > dealerScore) {
                System.out.println(players[i].getName() + " wins with " + playerScore + " points! (Dealer had " + dealerScore + " points)");
                players[i].winBet();
            } else if (playerScore < dealerScore) {
                System.out.println(players[i].getName() + " loses with " + playerScore + " points! (Dealer had " + dealerScore + " points)");
                players[i].loseBet();
            } else {
                System.out.println(players[i].getName() + " ties with the dealer at " + playerScore + " points!");
            }
        }

        // Reset hands and bets
        dealerHand.clear();
        for (Player player : players) {
            player.resetHand();
            player.resetBet();
        }
    }

    private void playTurn(Player player) {
        System.out.println(player.getName() + "'s turn:");
        while (true) {
            // Display hand
            System.out.println("Your hand:");
            player.getHand().printCards();

            // Check if player has blackjack or bust
            int score = getHandScore(player.getHand());
            if (score == 21) {
                System.out.println("You have blackjack!");
                break;
            } else if (score > 21) {
                System.out.print("Do you want to hit or stand? ");
        String input = scanner.nextLine();
        while (!input.equals("hit") && !input.equals("stand")) {
            System.out.print("Invalid input. Do you want to hit or stand? ");
            input = scanner.nextLine();
        }

        // Handle hit
        if (input.equals("hit")) {
            player.getHand().add(deck.takeFromTop());
        } else { // Stand
            break;
        }
    }

    // Handle split option
    if (player.getHand().getNumCards() == 2 && player.getHand().getCard(0).getRank() == player.getHand().getCard(1).getRank()) {
        System.out.print("Do you want to split your hand? ");
        String input = scanner.nextLine();
        while (!input.equals("yes") && !input.equals("no")) {
            System.out.print("Invalid input. Do you want to split your hand? ");
            input = scanner.nextLine();
        }
        if (input.equals("yes")) {
            DynamicCardArray hand1 = new DynamicCardArray();
            DynamicCardArray hand2 = new DynamicCardArray();
            hand1.add(player.getHand().takeFromTop());
            hand2.add(player.getHand().takeFromTop());
            hand1.add(deck.takeFromTop());
            hand2.add(deck.takeFromTop());
            Player[] newPlayers = new Player[players.length + 1];
            int[] newBets = new int[playerBets.length + 1];
            for (int i = 0; i < players.length; i++) {
                newPlayers[i] = players[i];
                newBets[i] = playerBets[i];
            }
            newPlayers[players.length] = new Player(player.getName() + " (hand 2)", hand2);
            newBets[playerBets.length] = playerBets[player.getIndex()];
            players = newPlayers;
            playerBets = newBets;
            player.setHand(hand1);
            playTurn(players[players.length - 1]);
        }
    }
}

private void playDealerTurn() {
    System.out.println("Dealer's turn:");
    while (true) {
        // Display hand
        System.out.println("Dealer's hand:");
        dealerHand.printCards();

        // Check if dealer has blackjack or bust
        int score = getHandScore(dealerHand);
        if (score == 21) {
            System.out.println("Dealer has blackjack!");
            break;
        } else if (score > 21) {
            System.out.println("Dealer busts with " + score + " points!");
            break;
        }

        // Dealer hits if score is less than 17
        if (score < 17) {
            dealerHand.add(deck.takeFromTop());
            System.out.println("Dealer hits.");
        } else {
            System.out.println("Dealer stands.");
            break;
        }
    }
}

private int getHandScore(DynamicCardArray hand) {
    int numAces = 0;
    int score = 0;

    for (int i = 0; i < hand.getNumCards(); i++) {
        Card card = hand.getCard(i);
        if (card.getRank() == Rank.ACE) {
            numAces++;
            score += 11;
        } else if (card.getRank() == Rank.JACK || card.getRank() == Rank.QUEEN || card.getRank() == Rank.KING) {
            score += 10;
        } else {
            score += card.getRank