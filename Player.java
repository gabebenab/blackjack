import java.util.ArrayList;

public class Player {
    private DynamicCardArray hand;
    private int bank;
    private int bet;
    private boolean hasSplit;
    private DynamicCardArray splitHand;
    
    public Player() {
        hand = new DynamicCardArray();
        bank = 1000; // starting bank with $1000
        bet = 0;
        hasSplit = false;
        splitHand = new DynamicCardArray();
    }
    
    public void addToHand(Card c) {
        hand.add(c);
    }
    
    public void clearHand() {
        hand.clear();
    }
    
    public int getHandValue() {
        return hand.getValue();
    }
    
    public boolean hasBlackjack() {
        return hand.hasBlackjack();
    }
    
    public boolean hasBusted() {
        return hand.hasBusted();
    }
    
    public int getBank() {
        return bank;
    }
    
    public void addToBank(int amount) {
        bank += amount;
    }
    
    public void removeFromBank(int amount) {
        bank -= amount;
    }
    
    public int getBet() {
        return bet;
    }
    
    public void setBet(int amount) {
        bet = amount;
    }
    
    public boolean canDoubleDown() {
        return (bet <= bank);
    }
    
    public void doubleBet() {
        bank -= bet;
        bet *= 2;
    }
    
    public boolean canSplit() {
        if (hand.getSize() != 2) {
            return false;
        }
        
        Card card1 = hand.getCard(0);
        Card card2 = hand.getCard(1);
        
        return (card1.getRank() == card2.getRank() && bank >= bet);
    }
    
    public void split() {
        hasSplit = true;
        splitHand.add(hand.takeFromTop());
        bet *= 2;
    }
    
    public boolean hasSplit() {
        return hasSplit;
    }
    
    public DynamicCardArray getHand() {
        return hand;
    }
    
    public DynamicCardArray getSplitHand() {
        return splitHand;
    }
}